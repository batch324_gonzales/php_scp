<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>S01 Activity</title>
	</head>

	<body>
		<!-- Activity 1:  -->
		<h1>Full Address</h1>
		<p><?php echo getFullAddress('3F', 'Caswyn Bldg.', 'Timog Avenue', 'Quezon City', 'Metro Manila', 'Philippines'); ?></p>
		<p><?php echo getFullAddress('3F', 'Enzo Bldg.', 'Buendia Avenue', 'Makati City', 'Metro Manila', 'Philippines'); ?></p>


		<!-- Activity 2:  -->
		<h1>Letter-Based Grading</h1>
		<p><?php echo getLetterGrade(87); ?></p>
		<p><?php echo getLetterGrade(94); ?></p>
		<p><?php echo getLetterGrade(74); ?></p>


	</body>
</html>