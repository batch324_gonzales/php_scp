<?php 


class Person {
	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($firstName, $middleName, $lastName){
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	public function printName(){
		return "Your full name is $this->firstName $this->middleName. $this->lastName.";
	}

};

$person = new Person('Senku', 'D', 'Ishigami');


class Developer extends Person{
	public $profession;

	public function __construct($firstName, $middleName, $lastName, $profession){
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
		$this->profession = $profession;

	}

	public function printName(){
		return "Your name is $this->firstName $this->middleName $this->lastName and you are a $this->profession.";
	}

}

$developer = new Developer('John', 'Finch', 'Smith', 'developer');



class Engineer extends Person{
	public $profession;

	public function __construct($firstName, $middleName, $lastName, $profession){
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
		$this->profession = $profession;

	}

	public function printName(){
		return "Your are an $this->profession named $this->firstName $this->middleName $this->lastName.";
	}

}

$engineer = new Engineer('Harold', 'Myers', 'Reese', 'engineer');























?>