<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<title>S04:Access Modifiers and Encapsulation</title>
	</head>

	<body>
		<h1>Building</h1>
		<p><?php $building->setName('Caswynn Building'); ?></p>
		<p><?php echo $building->printName(); ?></p>
		<p><?php $building->getName(); ?></p>
		<p><?php $building->getFloors(); ?></p>
		<p><?php echo $building->printFloors(); ?></p>
		<p><?php echo $building->printAddress(); ?></p>
		<p><?php $building->setName('Caswynn Complex'); ?></p>
		<p><?php echo $building->printChangeName(); ?></p>


		<h1>Condominium</h1>
		<p><?php $condominium->setName('Enzo Condo'); ?></p>
		<p><?php echo $condominium->printName(); ?></p>
		<p><?php $condominium->getName(); ?></p>
		<p><?php $condominium->getFloors(); ?></p>
		<p><?php echo $condominium->printFloors(); ?></p>
		<p><?php echo $condominium->printAddress(); ?></p>
		<p><?php $condominium->setName('Enzo Tower'); ?></p>
		<p><?php echo $condominium->printChangeName(); ?></p>

		
		
	</body>
</html>