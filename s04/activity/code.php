<?php 

class Building {
    protected $name;
    protected $floors;
    protected $address;

    public function __construct($name, $floors, $address) {
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }

    public function getName() {
        return $this->name;
    }

    public function getFloors() {
        return $this->floors;
    }

    public function getAddress() {
        return $this->address;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function printName() {
        return "The name of the building is {$this->getName()}.";
    }

    public function printFloors() {
        return "The {$this->getName()} has {$this->getFloors()} floors.";
    }

    public function printAddress() {
        return "The {$this->getName()} is located at {$this->getAddress()}."; 
    }

    public function printChangeName() {
        return "The name of the building has been changed to {$this->getName()}."; 
    }
}



class Condominium extends Building {

    public function __construct($name, $floors, $address) {
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }


    public function printName() {
        return "The name of the condominium is {$this->getName()}.";
    }

    public function printFloors() {
        return "The {$this->getName()} has {$this->getFloors()} floors.";
    }

    public function printAddress() {
        return "The {$this->getName()} is located at {$this->getAddress()}."; 
    }

    public function printChangeName() {
        return "The name of the building has been changed to {$this->getName()}."; 
    }
}


$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');


$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines', 500);





















?>