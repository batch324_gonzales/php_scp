<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>S02 Activity</title>
	</head>

	<body>
		<!-- Loops -->
		<h1>Divisibles of Five</h1>
		<?php numbersDivisibleBy5(); ?>


		<!-- Arrays -->
		<h1>Array Manipulation</h1>
		<p><?php add_student("John Smith"); ?></p>
		<p><?php print_r(var_dump($students)); ?></p>
		<p><?php echo count($students); ?></p>

		<p><?php add_student("Jane Smith"); ?></p>
		<p><?php print_r(var_dump($students)); ?></p>
		<p><?php echo count($students); ?></p>

		<?php array_pop($students); ?>
		<p><?php print_r(var_dump($students)); ?></p>
		<p><?php echo count($students); ?></p>


	</body>
</html>