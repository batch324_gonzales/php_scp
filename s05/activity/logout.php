<?php
session_start();

// Upon login of correct credentials, show the email and a logout button.
if (!isset($_SESSION['username'])) {
    header("Location: login.php");
    exit();
}

// Once the logout button is clicked, clear the user information and show the login again.
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    session_unset();
    session_destroy();
    header("Location: login.php");
    exit();
}
?>

<!DOCTYPE html>
<html>

<head>
    <title>Logout</title>
</head>

<body>
    <!-- Show welcome message and logout button -->
    <p>Hello, <?php echo $_SESSION['username']; ?></p>
    <form method="post" action="logout.php">
        <button type="submit">Logout</button>
    </form>
</body>

</html>
