<?php
session_start();

// Create a login page that takes a username and password.
if (isset($_SESSION['username'])) {
    header("Location: logout.php");
    exit();
}

function validateCredentials($username, $password) {
    $validUsername = 'johnsmith@gmail.com';
    $validPassword = '1234';

    return $username === $validUsername && $password === $validPassword;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $username = $_POST['username'];
    $password = $_POST['password'];

    if (validateCredentials($username, $password)) {
        $_SESSION['username'] = $username;
        header("Location: logout.php");
        exit();
    } else {
        $error = "Invalid username or password!";
    }
}
?>

<!DOCTYPE html>
<html>

<head>
    <title>Login Page</title>
</head>

<body>
    <?php if (isset($error)) { ?>
        <p><?php echo $error; ?></p>
    <?php } ?>
    <!-- Login form -->
    <h2>Login</h2>
    <form method="post" action="login.php">
        <label for="username">Email:</label>
        <input type="text" name="username" id="username" required><br>
        <label for="password">Password:</label>
        <input type="password" name="password" id="password" required><br>
        <button type="submit">Login</button>
    </form>
</body>

</html>
